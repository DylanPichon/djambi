import const


def string_impossible_choice_color(board):
    """Méthode appellée pour renvoyer un message quand on ne peut pas se déplacer sur un de nos pions"""
    string = "Impossible de se déplacer sur un de vos pions"
    write(string, board)
    return string


def string_choice(board_cell, i, board):
    """Méthode appellée pour renvoyer un message avec les infos de clic"""
    string = "Vous avez cliqué sur " + \
             board_cell.pions[i].__class__.__name__ + \
             " " + \
             board_cell.pions[i].color.nom + \
             " en " + \
             str(board_cell.x + 1) + \
             "-" + \
             str(board_cell.y + 1)
    write(string, board)
    return string


def string_move(board, board_cell, i):
    """Méthode appellée pour renvoyer un message avec les infos de déplacement"""
    string = "Vous avez placé " + \
             board.cells[board_cell.x][board_cell.y].pions[i].__class__.__name__ + \
             " " + \
             board.cells[board_cell.x][board_cell.y].pions[i].color.nom + \
             " en " + \
             str(board.cells[board_cell.x][board_cell.y].x + 1) + \
             "-" + \
             str(board.cells[board_cell.x][board_cell.y].y + 1)
    write(string, board)
    return string


def write(txt, board):
    """Méthode appellée pour ecrire dans le Canvas"""
    if board.nbAffichageCanva >= const.NB_AFFICHAGE_MAX_CANVA:
        board.canvas.delete("all")
        board.canvaPlace = const.Y_START_POSITION_CANVA
        board.nbAffichageCanva = 0
    board.canvas.create_text(const.X_POSITION_CANVA, board.canvaPlace, text=txt, font=const.FONT_CANVA,
                             fill=const.FONT_COLOR_CANVA)
    board.canvaPlace = board.canvaPlace + const.STEP_CANVA
    board.nbAffichageCanva += 1
