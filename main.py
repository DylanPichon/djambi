from classes.board import Board
from const import *

from tkinter import *

if __name__ == '__main__':
    fenetre = Tk()
    board = Board(BOARD_DIMENSIONS, fenetre)
    board.instanciate()
    board.draw(fenetre)
    fenetre.mainloop()
