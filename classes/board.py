from classes.assassin import Assassin
from classes.boardcell import BoardCell
from classes.chef import Chef
from classes.couleur import Couleur
from classes.diplomate import Diplomate
from classes.militant import Militant
from classes.necromobile import Necromobile
from classes.reporter import Reporter
from utils import *
from tkinter import *


class Board:

    canvaPlace = const.Y_START_POSITION_CANVA
    nbAffichageCanva = 0
    celluleCliquee = None

    def __init__(self, dimensions: tuple, fenetre: Tk):
        """Initialie le board"""
        self.dimensions = dimensions
        cells = []
        for x in range(self.dimensions[0]):
            row = []
            for y in range(self.dimensions[1]):
                row.append(BoardCell(x, y, [], self))
            cells.append(row)
        self.cells = cells
        self.canvas = Canvas(fenetre, background=const.BACKGROUND_CANVA, width=const.WIDTH_CANVA, height=const.HEIGHT_CANVA)
        self.canvas.grid(rowspan=const.ROWSPAN_CANVA)

    def move(self, boardCell: BoardCell, fenetre: Tk):
        """Deplace un pion vers la case voulue"""
        self.cells[boardCell.x][boardCell.y].pions = self.celluleCliquee.pions
        self.cells[self.celluleCliquee.x][self.celluleCliquee.y].pions = []
        self.celluleCliquee = None
        self.draw(fenetre)
        for i in range(len(self.cells[boardCell.x][boardCell.y].pions)):
            string_move(self, boardCell, i)

    def draw(self, fenetre: Tk):
        """Dessine le board"""
        for i in range(len(self.cells)):
            for j in range(len(self.cells[i])):
                self.cells[i][j].draw(fenetre)

    def instanciate(self):
        """Instancie les pieces dans le board"""
        vert = Couleur(const.LIBELLE_COULEUR_VERT, const.CODE_COULEUR_VERT)
        bleu = Couleur(const.LIBELLE_COULEUR_BLEU, const.CODE_COULEUR_BLEU)
        rouge = Couleur(const.LIBELLE_COULEUR_ROUGE, const.CODE_COULEUR_ROUGE)
        jaune = Couleur(const.LIBELLE_COULEUR_JAUNE, const.CODE_COULEUR_JAUNE)

        ########################################### verts
        chefVert = Chef(vert)
        self.cells[0][0].pions = [chefVert]

        reporterVert = Reporter(vert)
        self.cells[0][1].pions = [reporterVert]

        militantVert1 = Militant(vert)
        self.cells[0][2].pions = [militantVert1]

        assassinVert = Assassin(vert)
        self.cells[1][0].pions = [assassinVert]

        diplomateVert = Diplomate(vert)
        self.cells[1][1].pions = [diplomateVert]

        militantVert2 = Militant(vert)
        self.cells[1][2].pions = [militantVert2]

        militantVert3 = Militant(vert)
        self.cells[2][0].pions = [militantVert3]

        militantVert4 = Militant(vert)
        self.cells[2][1].pions = [militantVert4]

        necromobileVert = Necromobile(vert)
        self.cells[2][2].pions = [necromobileVert]
        #####################################################

        ############################################### Rouges
        militantRouge1 = Militant(rouge)
        self.cells[0][6].pions = [militantRouge1]

        reporterRouge = Reporter(rouge)
        self.cells[0][7].pions = [reporterRouge]

        chefRouge = Chef(rouge)
        self.cells[0][8].pions = [chefRouge]

        militantRouge2 = Militant(rouge)
        self.cells[1][6].pions = [militantRouge2]

        diplomateRouge = Diplomate(rouge)
        self.cells[1][7].pions = [diplomateRouge]

        assassinRouge = Assassin(rouge)
        self.cells[1][8].pions = [assassinRouge]

        necromobileRouge = Necromobile(rouge)
        self.cells[2][6].pions = [necromobileRouge]

        militantRouge3 = Militant(rouge)
        self.cells[2][7].pions = [militantRouge3]

        militantRouge4 = Militant(rouge)
        self.cells[2][8].pions = [militantRouge4]
        #################################################################

        ##################################################Jaunes
        militantJaune3 = Militant(jaune)
        self.cells[6][0].pions = [militantJaune3]

        militantJaune4 = Militant(jaune)
        self.cells[6][1].pions = [militantJaune4]

        necromobileJaune = Necromobile(jaune)
        self.cells[6][2].pions = [necromobileJaune]

        assassinJaune = Assassin(jaune)
        self.cells[7][0].pions = [assassinJaune]

        diplomateJaune = Diplomate(jaune)
        self.cells[7][1].pions = [diplomateJaune]

        militantJaune2 = Militant(jaune)
        self.cells[7][2].pions = [militantJaune2]

        chefJaune = Chef(jaune)
        self.cells[8][0].pions = [chefJaune]

        reporterJaune = Reporter(jaune)
        self.cells[8][1].pions = [reporterJaune]

        militantJaune1 = Militant(jaune)
        self.cells[8][2].pions = [militantJaune1]
        #####################################################################

        ####################################################### Bleus
        necromobileBleu = Necromobile(bleu)
        self.cells[6][6].pions = [necromobileBleu]

        militantBleu3 = Militant(bleu)
        self.cells[6][7].pions = [militantBleu3]

        militantBleu4 = Militant(bleu)
        self.cells[6][8].pions = [militantBleu4]

        militantBleu1 = Militant(bleu)
        self.cells[7][6].pions = [militantBleu1]

        diplomateBleu = Diplomate(bleu)
        self.cells[7][7].pions = [diplomateBleu]

        assassinBleu = Assassin(bleu)
        self.cells[7][8].pions = [assassinBleu]

        militantBleu2 = Militant(bleu)
        self.cells[8][6].pions = [militantBleu2]

        reporterBleu = Reporter(bleu)
        self.cells[8][7].pions = [reporterBleu]

        chefBleu = Chef(bleu)
        self.cells[8][8].pions = [chefBleu]


