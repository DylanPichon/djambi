from tkinter import Button
from PIL import Image, ImageTk
import utils
import const
from tkinter import Tk


class BoardCell:

    def __init__(self, x: int, y: int, pions: [], board):
        """Initialise la boardCell"""
        self.pions = pions
        self.x = x
        self.y = y
        self.board = board

    def onClickCell(self, fenetre: Tk):
        """Fonction appellée quand on clique sur une cellule"""
        if self.board.celluleCliquee is None:
            if self.pions:
                self.board.celluleCliquee = self
                for i in range(len(self.pions)):
                    utils.string_choice(self, i, self.board)
        else:
            if self.pions:
                if self.pions[0].color == self.board.celluleCliquee.pions[0].color:
                    if self != self.board.celluleCliquee:
                        utils.string_impossible_choice_color(self.board)
                    self.board.celluleCliquee = None
                else:
                    self.board.move(self, fenetre)
            else:
                self.board.move(self, fenetre)

    def draw(self, fenetre: Tk):
        """Dessine la cellule"""
        if self.pions:
            for pion in self.pions:
                image = Image.open(pion.image())
                image = image.resize((const.HEIGHT_IMAGE, const.WIDTH_IMAGE))
                imageTk = ImageTk.PhotoImage(image)
                cell = Button(fenetre, image=imageTk, bg=pion.color.code,
                              command=lambda: self.onClickCell(fenetre))
                cell.image = imageTk
        else:
            cell = Button(fenetre, height=const.HEIGHT_EMPTY_CELL, width=const.WIDTH_EMPTY_CELL,
                          command=lambda: self.onClickCell(fenetre))
        cell.grid(row=self.x, column=self.y + 1)


