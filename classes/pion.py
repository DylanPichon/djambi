from abc import ABC, abstractmethod

from classes.couleur import Couleur


class Pion(ABC):

    def __init__(self, color: Couleur):
        """Initialise le pion"""
        self.alive = True
        self.color = color

    @abstractmethod
    def availables_moves(self):
        pass

    @abstractmethod
    def after_move(self):
        pass

